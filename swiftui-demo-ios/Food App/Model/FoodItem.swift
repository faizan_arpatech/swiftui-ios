//
//  FoodItem.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 14/11/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import Foundation
import SwiftUI

struct FoodItem: Identifiable {
    
    var id: String
    var item_name: String
    var item_cost: NSNumber
    var item_details: String
    var item_image: String
    var item_ratings: NSNumber
}
