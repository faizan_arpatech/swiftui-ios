//
//  FoodHomeVM.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 14/11/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI
import CoreLocation
import FirebaseAuth
import FirebaseFirestore

class FoodHomeVM: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var search = "" {
        didSet {
            if search == "" {
                withAnimation(.easeIn, {
                    filteredItems = items
                })
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.filterData()
                }
            }
        }
    }
    
    @Published var locationManager = CLLocationManager()
    
    @Published var userLocation : CLLocation!
    @Published var userAddress = ""
    @Published var noLocation = false
    
    @Published var showMenu: Bool = false
    
    @Published var items = [FoodItem]()
    @Published var filteredItems = [FoodItem]()
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            print("authorized")
            manager.requestLocation()
            noLocation = false
        case .denied:
            print("denied")
            noLocation = true
        default:
            print("unknown")
            noLocation = false
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.last {
            self.userLocation = location
            self.extractLocation()
        }
    }
    
    func extractLocation() {
        CLGeocoder().reverseGeocodeLocation(self.userLocation) { res, err in
            guard let data = res else { return }
            
            var address = ""
            
            address += data.first?.name ?? ""
            address += ", "
            address += data.first?.locality ?? ""
            self.userAddress = address
        }
    }
    
    func login() {
        Auth.auth().signInAnonymously { (res, err) in
            if let err = err {
                print(err.localizedDescription)
            }
            self.fetchData()
        }
    }
    
    func fetchData() {
        let db = Firestore.firestore()
        db.collection("items").getDocuments { (snapshot, err) in
            guard let itemData = snapshot else { return }
            
            self.items = itemData.documents.compactMap({ (doc) -> FoodItem? in
                
                let id = doc.documentID
                let name = doc.get("item_name") as! String
                let cost = doc.get("item_cost") as! NSNumber
                let ratings = doc.get("item_ratings") as! NSNumber
                let image = doc.get("item_image") as! String
                let detail = doc.get("item_details") as! String
                
                return FoodItem(id: id, item_name: name, item_cost: cost, item_details: detail, item_image: image, item_ratings: ratings)
            })
            self.filteredItems = self.items
        }
    }
    
    func filterData() {
        withAnimation(.easeIn, {
            self.filteredItems = self.items.filter {
                return $0.item_name.lowercased().contains(self.search.lowercased())
            }
        })
    }
}
