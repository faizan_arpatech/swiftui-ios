//
//  Menu.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 14/11/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct Menu: View {
    
    @ObservedObject var homeData: FoodHomeVM
    
    var body: some View {
        VStack {
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                HStack(spacing: 15) {
                    Image(systemName: "cart")
                        .font(.title)
                        .foregroundColor(Color.red)
                    
                    Text("Cart")
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                    Spacer()
                }
                .padding()
            })
            Spacer()
            HStack {
                Spacer()
                Text("Version 0.1")
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.red)
            }
            .padding()
        }
        .padding([.top, .trailing])
        .frame(width: UIScreen.main.bounds.width / 1.6)
        .background(Color.white)
    }
}

struct Menu_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
