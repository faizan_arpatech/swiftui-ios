//
//  FoodHome.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 14/11/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct FoodHome: View {
    
    @ObservedObject var HomeModel = FoodHomeVM()
    
    var body: some View {
        ZStack {
            
            VStack(spacing: 10) {
                HStack(spacing: 15) {
                    Button(action: {
                        withAnimation(.easeIn) {
                            HomeModel.showMenu.toggle()
                        }
                    }, label: {
                        Image(systemName: "line.horizontal.3")
                            .font(.title)
                            .foregroundColor(.red)
                    })
                    Text(HomeModel.userLocation == nil ? "Locating..." : "Deliver To")
                        .foregroundColor(.black)
                    Text(HomeModel.userAddress)
                        .font(.caption)
                        .fontWeight(.heavy)
                        .foregroundColor(.red)
                    Spacer()
                }
                .padding([.horizontal, .top])
                
                Divider()
                
                HStack(spacing: 15) {
                    TextField("Search", text: $HomeModel.search)
                    
                    if HomeModel.search != "" {
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Image(systemName: "magnifyingglass")
                                .font(.body)
                                .foregroundColor(.gray)
                        })
                        .animation(.easeIn)
                    }
                }
                .padding(.horizontal)
                .padding(.top, 10)
                
                Divider()
                
                ScrollView(.vertical, showsIndicators: false, content: {
                    VStack(spacing: 25) {
                        ForEach(HomeModel.filteredItems) { item in
                            FoodItemView(item: item)
                        }
                    }
                    .padding(.top, 10)
                })
            }
            
            HStack {
                Menu(homeData: HomeModel)
                    .offset(x: HomeModel.showMenu ? 0 : -UIScreen.main.bounds.width / 1.6)
                Spacer()
            }
            .background(Color.black.opacity(HomeModel.showMenu ? 0.3 : 0)
                            .onTapGesture(perform: {
                                withAnimation(.easeIn) {
                                    HomeModel.showMenu.toggle()
                                }
                            })
            )
            
            // Alert if permission denied
            if HomeModel.noLocation {
                Text("Please Enbale Location Access In Settings to further move on!!!")
                    .foregroundColor(.black)
                    .frame(width: UIScreen.main.bounds.width - 100, height: 120)
                    .background(Color.white)
                    .cornerRadius(10)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color.black.opacity(0.3))
            }
        }
        .onAppear(perform: {
            HomeModel.locationManager.delegate = HomeModel
            HomeModel.login()
        })
    }
}

struct FoodHome_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            FoodHome()
        }
    }
}


struct FoodItemView: View {
    
    var item: FoodItem
    
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .center, vertical: .top), content: {
            VStack {
                WebImage(url: URL(string: item.item_image))
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: UIScreen.main.bounds.width - 30, height: 250)
                HStack(spacing: 8) {
                    Text(item.item_name)
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                    Spacer()
                    // For Rating View
                    ForEach(1...5, id: \.self) { index in
                        Image(systemName: "star.fill")
                            .foregroundColor(index <= Int(truncating: item.item_ratings) ? Color.red : Color.gray)
                    }
                }
                HStack {
                    Text(item.item_details)
                        .font(.caption)
                        .foregroundColor(.gray)
                        .lineLimit(2)
                    Spacer()
                }
            }
            
            HStack {
                Text("Free Delivery")
                    .foregroundColor(.white)
                    .padding(.vertical, 10)
                    .padding(.horizontal)
                    .background(Color.red)
                Spacer()
                Button(action: {}, label: {
                    Image(systemName: "plus")
                        .foregroundColor(.white)
                        .padding(10)
                        .background(Color.red)
                        .clipShape(Circle())
                })
            }
            .padding([.top, .trailing], 10)
        })
        .frame(width: UIScreen.main.bounds.width - 30)
    }
}
