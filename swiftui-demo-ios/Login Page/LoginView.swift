//
//  LoginView.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 18/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

let lightGreyColor = Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0)


struct LoginView: View {
    
    @State var username = ""
    @State var password = ""
    @State var authenticationDidFail: Bool = true
    @State var editingMode: Bool = false
    
    var body: some View {
        ZStack {
            VStack {
                WelcomeText()
                HeaderImage()
                UsernameTextField(username: $username, editingMode: $editingMode)
                PasswordTextField(password: $password, editingMode: $editingMode)
                if authenticationDidFail {
                    Text("Information not correct. Try again.")
                        .offset(y: -10)
                        .foregroundColor(.red)
                }
                Button(action: {
                    
                }) {
                    LoginButtonContent()
                }
            }
            .padding()
        }
        .offset(y: editingMode ? -150 : 0)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

struct WelcomeText: View {
    var body: some View {
        Text("Welcome!").font(.largeTitle)
    }
}

struct HeaderImage: View {
    var body: some View {
        Image("pizza")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 150, height: 150)
            .clipped()
            .cornerRadius(150)
            .padding(.bottom, 50)
    }
}

struct UsernameTextField: View {
    
    @Binding var username: String
    @Binding var editingMode: Bool
    
    var body: some View {
        TextField("Username", text: $username, onEditingChanged: { edit in
            if edit {
                self.editingMode = true
            } else {
                self.editingMode = false
            }
        })
        .padding()
        .background(lightGreyColor)
        .cornerRadius(5.0)
        .padding(.bottom, 15)
    }
}

struct PasswordTextField: View {
    
    @Binding var password: String
    @Binding var editingMode: Bool
    
    var body: some View {
        SecureField("Password", text: $password)
        .padding()
        .background(lightGreyColor)
        .cornerRadius(5.0)
        .padding(.bottom, 15)
    }
}

struct LoginButtonContent: View {
    var body: some View {
        Text("Login")
            .font(.headline)
            .foregroundColor(.white)
            .padding()
            .frame(maxWidth: .infinity)
            .background(Color.green)
            .cornerRadius(15.0)
    }
}
