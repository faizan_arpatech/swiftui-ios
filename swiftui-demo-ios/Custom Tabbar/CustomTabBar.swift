//
//  CustomTabBar.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 05/11/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

var tabItems = ["Home", "Search", "Favourites", "Settings"]

struct CustomTabBar: View {
    var body: some View {
        HomeTab()
    }
}

struct CustomTabBar_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabBar()
    }
}

struct HomeTab: View {
    
    @State var selected = "Home"
    @State var centerX: CGFloat = 0
    
    init() {
        UITabBar.appearance().isHidden = true
    }
    
    var body: some View {
        VStack {
            TabView(selection: $selected) {
                Color.red
                    .tag(tabItems[0])
                    .ignoresSafeArea(.all, edges: .top)
                Color.blue
                    .tag(tabItems[1])
                    .ignoresSafeArea(.all, edges: .top)
                Color.black
                    .tag(tabItems[2])
                    .ignoresSafeArea(.all, edges: .top)
                Color.yellow
                    .tag(tabItems[3])
                    .ignoresSafeArea(.all, edges: .top)
            }
            
            // Custom Tabbar
            HStack {
                ForEach(tabItems, id: \.self) { value in
                    GeometryReader { reader in
                        TabBarButton(selected: $selected, centerX: $centerX, value: value, rect: reader.frame(in: .global))
                            .onAppear(perform: {
                                if value == tabItems.first {
                                    centerX = reader.frame(in: .global).midX
                                }
                            })
                    }
                    .frame(width: 70, height: 50)
                    
                    if value != tabItems.last { Spacer() }
                }
            }
            .padding(.horizontal, 25)
            .padding(.top)
            .padding(.bottom, UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 15 : UIApplication.shared.windows.first?.safeAreaInsets.bottom)
            .background(Color.white.clipShape(AnimatedShape(centerX: self.centerX)))
            .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: -5)
            .padding(.top, -15)
        }
        .ignoresSafeArea(.all, edges: .bottom)
    }
}

struct TabBarButton: View {
    
    @Binding var selected: String
    @Binding var centerX: CGFloat
    
    var value: String
    var rect: CGRect
    
    var body: some View {
        Button(action: {
            withAnimation(.spring()) {
                selected = value
                centerX = rect.midX
            }
        }, label: {
            VStack {
                Image("menu")
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: 26, height: 26)
                    .foregroundColor(selected == value ? .blue : .gray)
                Text(value)
                    .font(.caption)
                    .foregroundColor(.black)
                    .opacity(self.selected == value ? 1 : 0)
            }
            // Default Frame for Reading Mid X Axis Pro Curve....
            .padding(.top)
            .frame(width: 70, height: 50)
            .offset(y: selected == value ? -15 : 0)
        })
    }
}

struct AnimatedShape: Shape {
    
    var centerX: CGFloat = 0
    
    var animatableData: CGFloat {
        get { return centerX }
        set { centerX = newValue }
    }
    
    func path(in rect: CGRect) -> Path {
        return Path { path in
            path.move(to: CGPoint(x: 0, y: 15))
            path.move(to: CGPoint(x: 0, y: rect.height))
            path.move(to: CGPoint(x: rect.width, y: rect.height))
            path.move(to: CGPoint(x: rect.width, y: 15))
            
            path.move(to: CGPoint(x: centerX - 35, y: 15))
            path.addQuadCurve(to: CGPoint(x: centerX + 35, y: 15), control: CGPoint(x: centerX, y: -30))
        }
    }
}
