//
//  ContentView.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 17/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("My first SwiftUI app!")
                .font(.headline)
                .foregroundColor(.purple)
            HStack {
                Text("Thats Awesome!")
                Spacer()
                Text("Yeah!")
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
