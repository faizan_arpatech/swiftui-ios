//
//  ChatMessage.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 21/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import Foundation

class ChatMessage {
    let messageText: String
    let username: String
    let isMe: Bool
    let messageId = UUID()
    
    init(messageText: String, username: String) {
        self.messageText = messageText
        self.username = username
        if UserDefaults.standard.string(forKey: "username") == username {
            self.isMe = true
        } else {
            self.isMe = false
        }
    }
}
