//
//  ChatController.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 21/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class ChatController: ObservableObject {
    
    var messages = [ChatMessage]()
    let objectWillChange = PassthroughSubject<ChatController,Never>()
 
    func sendMessage(messageText: String) {
        let newChat = databaseChats.childByAutoId()
        let messageToSend = ["username": UserDefaults.standard.string(forKey: "username") ?? "Unknown user", "messageText": messageText]
        newChat.setValue(messageToSend)
    }
    
    func receiveMessages() {
        let query = databaseChats.queryLimited(toLast: 10)
        _ = query.observe(.childAdded) { [weak self] (snapshot) in
            if let data = snapshot.value as? [String: String], let retrievedUsername = data["username"], let retrievedMessageText = data["messageText"], !retrievedMessageText.isEmpty {
                
                let retrievedMessage = ChatMessage(messageText: retrievedMessageText, username: retrievedUsername)
                self?.messages.append(retrievedMessage)
                self?.objectWillChange.send(self!)
            }
        }
    }
}
