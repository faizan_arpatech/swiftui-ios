//
//  ChatDatabase.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 21/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import Foundation
import Firebase

let databaseRoot = Database.database().reference()
let databaseChats = databaseRoot.child("chats")
