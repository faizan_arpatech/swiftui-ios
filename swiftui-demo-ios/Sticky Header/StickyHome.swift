//
//  StickyHome.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 13/10/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct StickyHome: View {
    
    @State var time = Timer.publish(every: 0.1, on: .current, in: .tracking).autoconnect()
    @State var showHeader: Bool = false
    
    var headerHeight: CGFloat = UIScreen.main.bounds.height / 2.5
    
    var body: some View {
        
        ZStack(alignment: .top, content: {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    GeometryReader { g in
                        Image("pizza")
                            .resizable()
                            .offset(y: g.frame(in: .global).minY > 0 ? -g.frame(in: .global).minY: 0)
                            .frame(height: g.frame(in: .global).minY > 0 ? self.headerHeight + g.frame(in: .global).minY : self.headerHeight)
                            .onReceive(self.time) { (_) in
                                let y = g.frame(in: .global).minY
                                if -y > (self.headerHeight - 50) {
                                    withAnimation {
                                        self.showHeader = true
                                    }
                                } else {
                                    withAnimation {
                                        self.showHeader = false
                                    }
                                }
                            }
                    }
                    .frame(height: headerHeight)
                    VStack {
                        HStack {
                            Text("New Foods We Love")
                                .font(.title)
                                .fontWeight(.bold)
                            Spacer()
                            Button(action: {}, label: {
                                Text("See All")
                                    .fontWeight(.bold)
                            })
                        }
                        VStack(spacing: 20) {
                            ForEach(cardData) { card in
                                CardView(card: card)
                            }
                        }
                    }
                    .padding()
                    Spacer()
                }
            }
            if self.showHeader {
                TopView()
            }
        })
        .edgesIgnoringSafeArea(.top)
    }
}

struct StickyHome_Previews: PreviewProvider {
    static var previews: some View {
        StickyHome()
    }
}

struct TopView: View {
    var body: some View {
        
        HStack {
            VStack(alignment: .leading, spacing: 12) {
                HStack(alignment: .top) {
                    Image("pizza")
                        .resizable()
                        .frame(width: 25, height: 30)
                    Text("Arcade")
                        .font(.title)
                        .fontWeight(.bold)
                }
                Text("One month free, then $4.99/month.")
                    .font(.caption)
                    .foregroundColor(.gray)
            }
            Spacer()
            Button(action: {}, label: {
                Text("Try It Free")
                    .foregroundColor(.white)
                    .padding(.vertical, 10)
                    .padding(.horizontal, 25)
                    .background(Color.blue)
                    .clipShape(Capsule())
            })
        }
        .padding(.top, UIApplication.shared.windows.first!.safeAreaInsets.top == 0 ? 15 : UIApplication.shared.windows.first!.safeAreaInsets.top + 5)
        .padding(.horizontal)
        .padding(.bottom)
        .background(Color.white)
    }
}

struct CardView: View {
    
    var card: Card
    
    var body: some View {
        HStack(alignment: .top, spacing: 15) {
            Image(self.card.image)
                .frame(width: 80, height: 80)
                .clipped()
                .cornerRadius(10)
            VStack(alignment: .leading, spacing: 6) {
                Text(self.card.title)
                    .fontWeight(.bold)
                Text(self.card.subTitle)
                    .font(.caption)
                    .foregroundColor(.gray)
                HStack(spacing: 12) {
                    Button(action: {}, label: {
                        Text("GET")
                            .padding(.vertical, 10)
                            .padding(.horizontal, 15)
                            .background(Color.primary.opacity(0.05))
                            .clipShape(Capsule())
                    })
                    
                    Text("In App\nPurchases")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
            }
            Spacer()
        }
    }
}

struct Card: Identifiable {
    
    var id: Int
    var image: String
    var title: String
    var subTitle: String
}

var cardData = [
    Card(id: 0, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 1, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 2, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 3, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 4, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 5, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 6, image: "burger", title: "Test 1", subTitle: "Description"),
    Card(id: 7, image: "burger", title: "Test 1", subTitle: "Description")
]

struct BlurBG: UIViewRepresentable {
    
    func makeUIView(context: Context) -> UIVisualEffectView {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .systemMaterial))
        return view
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        
    }
}
