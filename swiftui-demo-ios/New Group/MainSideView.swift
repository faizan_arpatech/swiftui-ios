//
//  MainSideView.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 15/10/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct MainSideView: View {
    
    @State var index = "Home"
    @State var show = false
    
    var body: some View {
        ZStack {
            Color.black.opacity(self.show ? 0.05 : 0).edgesIgnoringSafeArea(.all)
            
            ZStack(alignment: .leading) {
                VStack(alignment: .leading, spacing: 20) {
                    HStack(spacing: 15) {
                        Image("pizza")
                            .resizable()
                            .frame(width: 65, height: 65)
                            .clipShape(Circle())
                        
                        VStack(alignment: .leading, spacing: 8) {
                            Text("Faizan Naseem")
                                .fontWeight(.bold)
                            Text("iOS Developer")
                        }
                    }
                    .padding(.bottom, 50)
                    
                    ForEach(slideMenus, id: \.self) { menu in
                        Button(action: {
                            withAnimation(.spring()) {
                                self.index = menu
                                self.show.toggle()
                            }
                        }, label: {
                            HStack {
                                Capsule()
                                    .fill(self.index == menu ? Color.orange : Color.clear)
                                    .frame(width: 5, height: 20)
                                Text(menu).padding(.leading)
                                    .foregroundColor(.black)
                            }
                        })
                    }
                    
                    Spacer()
                }
                    .padding(.leading)
                    .padding(.top)
                    .scaleEffect(self.show ? 1 : 0)
                
                ZStack(alignment: .topTrailing) {
                    ContainerView(show: self.$show, index: self.$index)
                        .scaleEffect(self.show ? 0.8 : 1)
                        .offset(x: self.show ? 150 : 0, y: self.show ? 50 : 0)
                        .disabled(self.show ? true : false)
                    
                    Button(action: {
                        withAnimation(.spring()) {
                            self.show.toggle()
                        }
                    }, label: {
                        Image(systemName: "xmark")
                            .resizable()
                            .frame(width: 15, height: 15)
                            .foregroundColor(.black)
                        
                        })
                        .padding()
                        .opacity(self.show ? 1 : 0)
                }
            }
        }
    }
}

struct MainSideView_Previews: PreviewProvider {
    static var previews: some View {
        MainSideView()
    }
}


struct ContainerView: View {
    
    @Binding var show: Bool
    @Binding var index: String
    
    var body: some View {
        VStack(spacing: 0) {
            ZStack {
                HStack {
                    Button(action: {
                        withAnimation(.spring()) {
                            self.show.toggle()
                        }
                    }, label: {
                        Image("menu")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(.black)
                    })
                    Spacer()
                }
                Text("Food")
                    .fontWeight(.semibold)
                    .font(.title)
            }
            .padding(.horizontal)
            .padding(.vertical, 10)
            
            ZStack {
                HomeView().opacity(self.index == "Home" ? 1 : 0)
                OrdersView().opacity(self.index == "Orders" ? 1 : 0)
                Wishlists().opacity(self.index == "Wishlists" ? 1 : 0)
            }
        }
        .background(Color.white)
        .cornerRadius(15)
    }
}


struct HomeView: View {
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 18) {
                ForEach(1...6, id: \.self) { i in
                    Image("pizza")
                        .resizable()
                        .frame(height: 200)
                        .cornerRadius(20)
                }
            }
            .padding()
        }
        .padding(.bottom)
    }
}

struct OrdersView: View {
    
    var body: some View {
        
        GeometryReader { g in
            VStack {
                Text("Orders")
            }
        }
    }
}

struct Wishlists: View {
    
    var body: some View {
        
        GeometryReader { g in
            VStack {
                Text("Wishlists")
            }
        }
    }
}

var slideMenus = ["Home", "Orders", "Wishlists"]
