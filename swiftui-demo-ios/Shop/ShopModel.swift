//
//  ShopModel.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 13/10/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import Foundation
import SwiftUI

struct BagModel: Identifiable {
    
    var id = UUID().uuidString
    var image: String
    var title: String
    var price: String
}

var bags = [
    BagModel(image: "Pizza", title: "Office Bag", price: "$234"),
    BagModel(image: "Pizza", title: "Office Bag", price: "$234"),
    BagModel(image: "Pizza", title: "Office Bag", price: "$234"),
    BagModel(image: "Pizza", title: "Office Bag", price: "$234"),
    BagModel(image: "Pizza", title: "Office Bag", price: "$234"),
    BagModel(image: "Pizza", title: "Office Bag", price: "$234"),
]

var scroll_tabs = ["Hand Bag", "Jewellary", "Footwear", "Dresses", "Beauty"]
