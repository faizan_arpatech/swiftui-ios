//
//  ShopHome.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 13/10/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct ShopHome: View {
    
    @State var selectedTab = scroll_tabs[0]
    
    var body: some View {
        VStack {
            ZStack {
                HStack(spacing: 15) {
                    Button(action: {}, label: {
                        Image(systemName: "line.horizontal.3.decrease")
                            .font(.title)
                            .foregroundColor(.black)
                    })
                    Spacer()
                    Button(action: {}, label: {
                        Image(systemName: "magnifyingglass")
                            .font(.title)
                            .foregroundColor(.black)
                    })
                    Button(action: {}, label: {
                        Image(systemName: "cart")
                            .font(.title)
                            .foregroundColor(.black)
                    })
                }
                Text("Shop")
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundColor(.black)
            }
            .padding()
            .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
            .background(Color.white)
            .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: 5)
            Spacer()
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    HStack {
                        Text("Women")
                            .font(.title)
                            .fontWeight(.heavy)
                            .foregroundColor(.black)
                        Spacer()
                    }
                    .padding()
                    // Horizontal Scrolling
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(spacing: 15) {
                            ForEach(scroll_tabs,id: \.self) { tab in
                                // Tab Button
                                TabButton(title: tab, selectedTab: self.$selectedTab)
                            }
                        }
                        .padding(.horizontal)
                        .padding(.top, 10)
                    }
                }
            }
        }
        .background(Color.black.opacity(0.05))
        .edgesIgnoringSafeArea(.all)
    }
}

struct ShopHome_Previews: PreviewProvider {
    static var previews: some View {
        ShopHome()
    }
}

struct TabButton: View {
    
    var title: String
    @Binding var selectedTab: String
    
    var body: some View {
        Button(action: {
            withAnimation(.spring()) {
                self.selectedTab = self.title
            }
        }, label: {
            VStack(alignment: .leading, spacing: 6) {
                Text(title)
                    .fontWeight(.heavy)
                    .foregroundColor(selectedTab == title ? .black : .gray)
                
                if selectedTab == title {
                    Capsule()
                        .fill(Color.black)
                        .frame(width: 40, height: 4)
                }
            }
            .frame(width: 100)
        })
    }
    
}
