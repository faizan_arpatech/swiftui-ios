//
//  DateLogin.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 30/09/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI
import Combine

struct DateLogin: View {
    var body: some View {
        DateLoginView()
    }
}

struct DateLogin_Previews: PreviewProvider {
    static var previews: some View {
        DateLogin()
    }
}

struct DateLoginView: View {
    
    @State var model = ModelData()
    
    var body: some View {
        VStack {
            Image("coke")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 150, height: 150)
                .clipped()
            VStack(spacing: 5) {
                HStack(spacing: 0) {
                    Text("Dating")
                        .font(.system(size: 35, weight: .heavy))
                        .foregroundColor(.red)
                    Text("Match")
                        .font(.system(size: 35, weight: .heavy))
                        .foregroundColor(.yellow)
                }
                Text("Lets choose your match")
                    .foregroundColor(Color.black.opacity(0.2))
                    .fontWeight(.heavy)
            }
            VStack(spacing: 20) {
                DateField(image: "person", placeholder: "Email", text: $model.email)
                DateField(image: "lock", placeholder: "Password", text: $model.password)
            }
            .padding(.top)
            
            Button(action: {
                
            }) {
                Text("Login")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding(.vertical)
                    .frame(width: UIScreen.main.bounds.width - 60)
                    .background(Color.red)
                    .clipShape(Capsule())
            }
            .padding(.top, 22)
            
            HStack(spacing: 10) {
                Text("Don't have an account?")
                    .foregroundColor(Color.red)
                Button(action: {}) {
                    Text("Sign Up Now")
                        .fontWeight(.bold)
                        .foregroundColor(.red)
                }
            }
            .padding(.top, 20)
            
            Spacer(minLength: 0)
            
            Button(action: {}) {
                Text("Forgot Password")
                    .fontWeight(.bold)
                    .foregroundColor(.red)
            }
        }
        .background(Color.white)
        .padding(EdgeInsets(top: 20, leading: 30, bottom: 20, trailing: 30))
    }
}

struct DateField: View {
    
    var image: String
    var placeholder: String
    @Binding var text: String
    
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .leading, vertical: .center)) {
            Image(systemName: image)
                .font(.system(size: 24))
                .foregroundColor(.white)
                .frame(width: 60, height: 60)
                .background(Color.red)
                .clipShape(Circle())
            TextField(placeholder, text: $text)
                .padding(.horizontal)
                .padding(.leading, 65)
                .frame(height: 60)
                .background(Color.gray.opacity(0.3))
        }
        .cornerRadius(30)
    }
}

//
class ModelData : ObservableObject {

    @Published var email = ""
    @Published var password = ""
    @Published var isSignUp = false
}
