//
//  DateSignUp.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 30/09/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct DateSignUp: View {
    
    @State var model = ModelData()
    
    var body: some View {
        VStack {
            Image("coke")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 150, height: 150)
                .clipped()
            VStack(spacing: 5) {
                HStack(spacing: 0) {
                    Text("Dating")
                        .font(.system(size: 35, weight: .heavy))
                        .foregroundColor(.red)
                    Text("Match")
                        .font(.system(size: 35, weight: .heavy))
                        .foregroundColor(.yellow)
                }
                Text("Lets choose your match")
                    .foregroundColor(Color.black.opacity(0.2))
                    .fontWeight(.heavy)
            }
            VStack(spacing: 20) {
                DateField(image: "person", placeholder: "Email", text: $model.email)
                DateField(image: "lock", placeholder: "Password", text: $model.password)
                DateField(image: "lock", placeholder: "Confirm Password", text: $model.password)
            }
            .padding(.top)
            
            Button(action: {
                
            }) {
                Text("Register")
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .padding(.vertical)
                    .frame(width: UIScreen.main.bounds.width - 60)
                    .background(Color.red)
                    .clipShape(Capsule())
            }
            .padding(.top, 22)
            Spacer(minLength: 0)
        }
        .background(Color.white)
        .padding(EdgeInsets(top: 20, leading: 30, bottom: 20, trailing: 30))
    }
}

struct DateSignUp_Previews: PreviewProvider {
    static var previews: some View {
        DateSignUp()
    }
}
