//
//  DetailRow.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 17/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct DetailRow: View {
    
    var food: Food
    @Binding var showOrderSheet: Bool
    
    var body: some View {
        HStack {
            VStack {
                Text(food.title)
                    .font(.headline)
                    .padding(.top, 10)
                Text("\(food.price, specifier: "%2.2f") $")
                    .font(.caption)
            }
            Spacer()
            Button(action: {
                self.showOrderSheet = true
            }) {
                Text("Order")
                    .foregroundColor(.white)
            }
            .frame(width: 80, height: 50, alignment: .center)
            .background(Color.orange)
            .cornerRadius(10.0)
        }
        .padding(20)
    }
}

struct DetailRow_Previews: PreviewProvider {
    static var previews: some View {
        DetailRow(food: foodData[0], showOrderSheet: .constant(false))
    }
}
