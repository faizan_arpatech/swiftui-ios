//
//  OrderForm.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 19/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct OrderForm: View {
    
    @State var specialRequests = true
    @Binding var showOrderSheet: Bool
    @State var specialRequestContent = ""
    @State var orderAmount = 1
    
    @State var firstName = ""
    @State var streetAddress = ""
    @State var city = ""
    @State var lastName = ""
    @State var userFeedback = 0.0
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Toggle(isOn: $specialRequests) {
                        Text("Any special requests?")
                    }
                    if specialRequests {
                        TextField("Enter your wishes", text: $specialRequestContent)
                    }
                    Stepper(value: $orderAmount, in: 1...10, label: {
                        Text("Number of items: \(orderAmount)")
                    })
                }
                Section {
                    TextField("First Name", text: $firstName)
                    TextField("Last Name", text: $lastName)
                    TextField("Street Address", text: $streetAddress)
                    TextField("City", text: $city)
                }
                Section {
                    Text("How happy were you with the userexperience?")
                    .padding(.top, 10)
                    HStack {
                        Image(systemName: "hand.thumbsup")
                        .resizable()
                        .frame(width: 20, height: 20)
                        Slider(value: $userFeedback, in: 0.0...10.0)
                        Image(systemName: "hand.thumbsdown")
                        .resizable()
                        .frame(width: 20, height: 20)
                    }
                }
                Section {
                    Button(action: {
                        self.showOrderSheet = false
                    }) {
                        Text("Place Order")
                    }
                }
            }
            .navigationBarTitle(Text("Welcome"))
        }
    }
}

struct OrderForm_Previews: PreviewProvider {
    static var previews: some View {
        OrderForm(showOrderSheet: .constant(true))
    }
}
