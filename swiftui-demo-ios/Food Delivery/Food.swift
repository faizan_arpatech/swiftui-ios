//
//  Food.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 17/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import Foundation
import SwiftUI

enum Categories {
    case burger
    case pasta
    case pizza
    case dessert
}

class Food: Identifiable {
    let title: String
    let price: Double
    let category: Categories
    let id: Int
    
    init(title: String, price: Double, category: Categories, id: Int) {
        self.title = title
        self.price = price
        self.category = category
        self.id = id
    }
}

