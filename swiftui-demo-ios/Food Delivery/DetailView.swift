//
//  DetailView.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 17/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    
    @State var showOrderSheet = false
    
    var currentCategory: Categories
    
    var body: some View {
        List(filterData(by: currentCategory)) { food in
            DetailRow(food: food, showOrderSheet: self.$showOrderSheet)
        }
        .navigationBarTitle("", displayMode: .inline)
        .sheet(isPresented: $showOrderSheet, content: {
            OrderForm(showOrderSheet: self.$showOrderSheet)
        })
    }
    
    func filterData(by category: Categories) -> [Food] {
        return foodData.filter({ $0.category == category })
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(currentCategory: .dessert)
    }
}
