//
//  OnBoardingContentView.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 19/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct OnBoardingContentView: View {
    
    var imageString: String
    
    var body: some View {
        Image(imageString)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .clipped()
    }
}

struct OnBoardingContentView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingContentView(imageString: "meditating")
    }
}
