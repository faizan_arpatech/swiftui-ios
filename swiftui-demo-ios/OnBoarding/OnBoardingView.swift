//
//  OnBoardingView.swift
//  swiftui-demo-ios
//
//  Created by Faizan Naseem on 19/08/2020.
//  Copyright © 2020 Faizan Naseem. All rights reserved.
//

import SwiftUI

struct OnBoardingView: View {
    
    @State var currentPageIndex = 0
    
    var subviews = [
        UIHostingController(rootView: OnBoardingContentView(imageString: "meditating")),
        UIHostingController(rootView: OnBoardingContentView(imageString: "skydiving")),
        UIHostingController(rootView: OnBoardingContentView(imageString: "sitting"))
    ]
    
    var titles = ["Take some time out", "Conquer personal hindrances", "Create a peaceful mind"]
     var captions = ["Take your time out and bring awareness into your everyday life", "Meditating helps you dealing with anxiety and other psychic problems", "Regular medidation sessions creates a peaceful inner mind"]
    
    var body: some View {
        VStack {
            PageViewController(currentPageIndex: $currentPageIndex, controllers: subviews)
                .frame(height: 450)
            Text(titles[currentPageIndex])
                .font(.title)
            Text(captions[currentPageIndex])
                .font(.subheadline)
                .foregroundColor(.gray)
                .multilineTextAlignment(.center)
                .frame(width: 300, height: 50, alignment: .center)
                .lineLimit(nil)
            HStack {
                PageControl(currentPageIndex: $currentPageIndex, numberOfPages: subviews.count)
                Spacer()
                Button(action: {
                    if self.currentPageIndex+1 == self.subviews.count {
                        self.currentPageIndex = 0
                    } else {
                        self.currentPageIndex += 1
                    }
                }) {
                    Image(systemName: "arrow.right")
                    .resizable()
                    .foregroundColor(.white)
                    .frame(width: 30, height: 30)
                    .padding()
                    .background(Color.orange)
                    .cornerRadius(30)
                }
            }
            .padding()
        }
    }
}

struct OnBoardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingView()
    }
}
